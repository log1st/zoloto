<?php
use yii\helpers\Html;

/**
 * @var $user \app\models\Users
 */
?>
<h1>Hello, <?=$user->name?></h1>
<h2>Here's your validation link:</h2>
<?=Html::a('CLICK ME!', [ 'auth/validate', 'code' => $user->validation ])?>
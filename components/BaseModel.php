<?php

namespace app\components;

use yii\db\ActiveRecord;

/**
 * Class BaseModel
 * @package app\components
 * @property integer $id
 */
class BaseModel extends ActiveRecord {
    public $old = [];
}
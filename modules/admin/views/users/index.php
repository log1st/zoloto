<?php
/**
 * @var Users[] $users
 */
use app\models\Users;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'List of users';

$f = (new Users);
$f->setAttributes(Yii::$app->request->get('Users'), false);
?>

<div class="btn-group">
    <?=Html::a('Create user', [ 'users/create' ], [
        'class' => 'btn btn-success'
    ])?>
</div>
<hr>
<?$items = [
    'id', 'name', 'phone', 'email',
    'role' => [
        'filter' => Html::dropDownList(Html::getInputName($f, 'role'), $f->role, array_merge([
                null => ' Choose...'
            ], $f->getListOfRoles()), [
            'class' => 'form-control'
        ])
    ]
]?>
<?$form = ActiveForm::begin([
    'id' => 'filter-form',
    'method' => 'get',
    'options' => [
        'action' => [ 'users/index' ],
        'autocomplete' => 'off'
    ]
])?>
    <table class="table table-hover table-bordered">
        <colgroup>
            <col width="50px">
        </colgroup>
        <thead>
        <tr>
            <?foreach($items as $key => $item):?>
                <th>
                    <?=$f->getAttributeLabel(is_numeric($key) ? $item : $key)?>
                </th>
            <?endforeach?>
            <th>Actions</th>
        </tr>
        <tr>
            <?foreach($items as $key => $item): $attr = is_numeric($key) ? $item : $key?>
                <th>
                    <?=$item['filter'] ?? Html::textInput(Html::getInputName($f, $attr), $f->{$attr}, [
                        'class' => 'form-control'
                    ])?>
                </th>
            <?endforeach?>
            <th><hr></th>
        </tr>
        </thead>
        <tbody>
        <?foreach($users as $user):?>
            <tr>
                <?foreach($items as $key => $item): $attr = is_numeric($key) ? $item : $key;?>
                    <td style="line-height: 32px">
                        <?=$user->getAttribute($attr)?>
                        <?if($attr == 'email'):?>
                            <?=Html::tag('i', '', [
                                'class' => 'glyphicon glyphicon-' . ($user->validation === null ? 'ok' : 'remove'),
                                'style' => 'line-height: 32px'
                            ])?>
                        <?endif?>
                    </td>
                <?endforeach?>
                <td>
                    <div class="btn-group">
                        <?=Html::a(Html::tag('i', '', [
                            'class' => 'glyphicon glyphicon-eye-open'
                        ]), [ 'users/view', 'id' => $user->id ], [
                            'class' => 'btn btn-success'
                        ])?>
                        <?=Html::a(Html::tag('i', '', [
                            'class' => 'glyphicon glyphicon-pencil'
                        ]), [ 'users/update', 'id' => $user->id ], [
                            'class' => 'btn btn-primary'
                        ])?>
                        <?=Html::a(Html::tag('i', '', [
                            'class' => 'glyphicon glyphicon-trash'
                        ]), [ 'users/delete', 'id' => $user->id ], [
                            'class' => 'btn btn-danger'
                        ])?>
                    </div>
                </td>
            </tr>
        <?endforeach?>
        </tbody>
    </table>
<?$form->end()?>
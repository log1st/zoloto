<?php
/**
 * @var $this \yii\web\View
 * @var Users $user
 */
use app\models\Users;
use yii\helpers\Html;

$this->title = 'View user ' . $user->name;
?>

<div class="btn-group">
    <?=Html::a('Update user', [ 'users/update', 'id' => $user->id ], [
        'class' => 'btn btn-primary'
    ])?>
    <?=Html::a('Delete user', [ 'users/delete', 'id' => $user->id ], [
        'class' => 'btn btn-danger'
    ])?>
</div>
<hr>
<h1>User <b><?=$user->name?></b></h1>
<?foreach([
              'name' => ucfirst($user->name),
              'phone' => $user->phone,
              'email' => Html::a($user->email, 'mailto:' . $user->email) . ' ' . Html::tag('i', '', [
                      'class' => $user->validation === null ? 'glyphicon glyphicon-ok' : 'glyphicon glyphicon-remove'
                  ]),
              'role' => ucfirst($user->role),
          ] as $key => $value):?>
    <dl class="dl-horizontal">
        <dt><?=$user->getAttributeLabel($key)?></dt>
        <dd><?=$value?></dd>
    </dl>
<?endforeach?>
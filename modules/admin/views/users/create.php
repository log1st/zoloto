<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\Users
 */

use yii\helpers\Html;

$this->title = 'Create user';
?>


<div class="btn-group">
    <?=Html::a('List of users', [ 'users/index' ], [
        'class' => 'btn btn-default'
    ])?>
</div>
<hr>

<?
echo $this->render('_form', [
    'model' => $model
]);
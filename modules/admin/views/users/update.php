<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\Users
 */

use yii\helpers\Html;

$this->title = 'Update user ' . $model->name;
?>

<div class="btn-group">
    <?=Html::a('View user', [ 'users/view', 'id' => $model->id ], [
        'class' => 'btn btn-default'
    ])?>
    <?=Html::a('Delete user', [ 'users/delete', 'id' => $model->id ], [
        'class' => 'btn btn-danger'
    ])?>
</div>
<hr>

<?
echo $this->render('_form', [
    'model' => $model
]);
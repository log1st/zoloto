<?php
namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Users;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\HttpException;

class UsersController extends Controller {
    public function actionIndex() {
        // Тут лучше бы вывести в SearchModel через ActiveDataProvider
        $query = Users::find();
        foreach(Yii::$app->request->get('Users') ?? [] as $key => $value) {
            if(mb_strlen($value)) {
                if (in_array($key, ['id', 'role']))
                    $query->andWhere([$key => $value]);
                else
                    $query->andFilterWhere(['like', $key, $value]);
            }
        }

        return $this->render('index', [
            'users' => $query->all()
        ]);
    }

    public function actionView($id) {
        $user = Users::findOne($id);
        if($user)
            return $this->render('view', [
                'user' => $user
            ]);
        throw new HttpException(404, 'User not found');
    }

    public function actionCreate() {
        $model = new Users([
            'scenario' => 'insert'
        ]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
            $model->save(false);
            $model->assignRole();
            $this->redirect(['users/view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model
        ]);
    }

    public function actionUpdate($id) {
        $model = Users::findOne($id);
        $model->setScenario('update');
        if($model) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->save(false);
                $model->assignRole(null, false, true);
                $this->redirect(['users/view', 'id' => $model->id]);
            }
            return $this->render('update', [
                'model' => $model
            ]);
        }
        throw new HttpException(404, 'User not found');
    }

    public function actionDelete($id) {
        $user = Users::findOne($id);
        if($user) {
            if(Yii::$app->user->identity->getId() <> $id) {
                $user->delete();
                return $this->redirect(['users/index']);
            }   else
                throw new HttpException(400, 'You can\'t delete yourself');
        }
        throw new HttpException(404, 'User not found');
    }
}
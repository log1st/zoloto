function encodeData(data) {
    return Object.keys(data).map(function(key) {
        return [key, data[key]].map(encodeURIComponent).join("=");
    }).join("&");
}

$(function() {
    $('#filter-form').change(function() {
        var list = {};
        $(this).find('th input,th select').each(function(i, input) {
            if($(input).val().length) {
                list[$(input).attr('name')] = $(input).val();
            }
        });
        location.href = location.origin + $(this).attr('action') + (Object.keys(list).length ? ('?' + encodeData(list)) : '');
    });
});
<?
    /**
     * @var $model \app\models\Users
     */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Login';

$form = ActiveForm::begin([
    'id' => 'users-form'
])?>

<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
    <?=$form->field($model, 'name')->textInput([
        'class' => 'form-control'
    ])?>

    <?=$form->field($model, 'password')->passwordInput([
        'class' => 'form-control'
    ])?>

    <?=Html::submitButton('Login', [
        'class' => 'btn btn-primary'
    ])?>
</div>

<?$form->end()?>
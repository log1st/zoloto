<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

$user = Yii::$app->user;
/** @var app\models\Users $dbUser */
$dbUser = $user->identity;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Zoloto',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    $config = [
        'options' => [
            'class' => 'navbar-nav navbar-right'
        ],
        'encodeLabels' => false,
        'items' => [
            [
                'label' => 'Home', 'url' => ['/site/index']
            ]
        ],
    ];
    if(($dbUser->role ?? null) == 'admin')
        $config['items'][] = [
            'label' => Html::tag('b', 'Admin-panel'), 'url' => (Yii::$app->controller->module->id ?? null) == 'admin' ? ['default/index'] : ['admin/default/index']
        ];
    if(Yii::$app->user->isGuest) {
        $config['items'] = array_merge($config['items'], [
            [ 'label' => 'Login', 'url' => ['auth/sign-in'] ],
            [ 'label' => 'Join', 'url' => ['auth/sign-up'] ],
        ]);
    }   else
        $config['items'][] = [
            'label' => 'Logout (' . $dbUser->name . ')', 'url' => ['/auth/log-out']
        ];
    echo Nav::widget($config);
    NavBar::end();
    ?>

    <div class="container">
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; log1st <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

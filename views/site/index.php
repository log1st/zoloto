<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$user = Yii::$app->user;
/** @var app\models\Users $dbUser */
$dbUser = $user->identity;

$this->title = 'Main';
?>
<?if($user->isGuest):?>
    You have to <?=Html::a('authorize', [ 'auth/sign-in' ])?>
<?else:?>
    <h2>Hello, <b><?=$dbUser->name?>!</b></h2>
    <?foreach([
        'name' => ucfirst($dbUser->name),
        'phone' => $dbUser->phone,
        'email' => Html::a($dbUser->email, 'mailto:' . $dbUser->email) . ' ' . Html::tag('i', '', [
                'class' => $dbUser->validation === null ? 'glyphicon glyphicon-ok' : 'glyphicon glyphicon-remove'
            ]),
        'role' => ucfirst($dbUser->role),
              ] as $key => $value):?>
    <dl class="dl-horizontal">
        <dt><?=$dbUser->getAttributeLabel($key)?></dt>
        <dd><?=$value?></dd>
    </dl>
    <?endforeach?>
<?endif?>

<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'zoloto',
    'basePath' => dirname(__DIR__),
    'components' => [
        'request' => [
            'cookieValidationKey' => 'zoloto',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'user' => [
            'identityClass' => 'app\models\Users',
            'loginUrl'=> [ 'auth/sign-in' ]
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                'admin' => 'admin/default/index',
                'login' => 'auth/sign-in',
                'join' => 'auth/sign-up',
                'logout' => 'auth/log-out',
                '<action:\w+>' => 'site/<action>',
                'admin/<controller:\w+>' => 'admin/<controller>/index',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

                '<module:\w+>/<controller:\w+>/<id:\d+>/' => '<module>/<controller>/view',
                '<module:\w+>/<controller:\w+>/<id:\d+>/<action:\w+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
            ],
        ]
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [ 'admin' ],
                    ]
                ]
            ],
        ]
    ],
    'params' => $params,
];

return $config;

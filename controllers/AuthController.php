<?php

namespace app\controllers;

use app\models\Users;
use yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class AuthController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [ 'sign-in', 'sign-up', 'log-out' ],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => [ 'log-out' ],
                        'roles' => [ '@' ],
                    ],
                    [
                        'allow' => true,
                        'actions' => [ 'sign-in', 'sign-up' ],
                        'roles' => [ '?' ],
                    ],
                ],
            ],
        ];
    }

    public function actionSignIn() {
        $model = new Users([
            'scenario' => 'sign-in'
        ]);

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $identity = Users::findOne([
                'name' => $model->name
            ]);
            if(Yii::$app->getSecurity()->validatePassword($model->password, $identity->password)) {
                $model->login($identity);
                $this->redirect(['site/index']);
            }   else
                $model->addError('password', 'Password is invalid');
        }

        return $this->render('login', [
            'model' => $model
        ]);
    }

    public function actionSignUp() {
        $model = new Users([
            'scenario' => 'sign-up'
        ]);

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
            $model->role = 'user';
            $model->validation = md5(uniqid() . 'sol');
            $model->save(false);
            $model = $model->findOne([
                'name' => $model->name
            ]);
            $model->sendValidationEmail();
            $model->login();
            $this->redirect(['site/index']);
        }

        return $this->render('join', [
            'model' => $model
        ]);
    }

    public function actionValidate($code) {
        $user = Users::findOne([
            'validation' => $code
        ]);
        if($user) {
            if(Yii::$app->user->isGuest || (!Yii::$app->user->isGuest && Yii::$app->user->id === $user->id)) {
                $user->validation = null;
                $user->save(false);
                if(Yii::$app->user->isGuest)
                    $user->login();
            }
        }
        $this->redirect(['site/index']);
    }

    public function actionLogOut() {
        if(!Yii::$app->user->isGuest)
            Yii::$app->user->logout();
        if(Yii::$app->request->isPost)
            return true;
        else
            return $this->redirect(['site/index']);
    }
}

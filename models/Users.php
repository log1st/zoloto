<?php

namespace app\models;

use app\components\BaseModel;
use yii;
use yii\web\IdentityInterface;

/**
 * Class Users
 * @package app\models
 * @property integer $id
 * @property string $name
 * @property string $password
 * @property string $phone
 * @property string $email
 * @property string $role
 * @property string $validation
 */
class Users extends BaseModel implements IdentityInterface {
    public $passwordConfirm = null;

    static function tableName() {
        return 'users';
    }

    public function scenarios() {
        return array_merge(parent::scenarios(), [
            'sign-in' => [ 'name', 'password' ],
            'sign-up' => [ 'name', 'password', 'passwordConfirm', 'phone', 'email' ],
            'insert' => [ 'name', 'password', 'phone', 'email', 'role' ],
            'update' => [ 'name', 'password', 'phone', 'email', 'role' ],
        ]);
    }

    public function rules() {
        return [
            // Sign In
            [ [ 'name', 'password' ], 'required', 'on' => [ 'sign-in', 'insert' ] ],
            [ [ 'name' ], 'exist', 'on' => 'sign-in' ],

            // Sign Up
            [ [ 'name', 'password', 'passwordConfirm', 'phone', 'email' ], 'required', 'on' => 'sign-up' ],
            [ 'passwordConfirm', 'compare', 'compareAttribute' => 'password', 'on' => 'sign-up' ],

            // Insert / Update
            [ [ 'name', 'phone', 'email', 'role' ], 'required', 'on' => [ 'insert', 'update' ] ],
            [ 'role', 'in', 'range' => array_keys(Users::getListOfRoles()), 'on' => [ 'insert', 'update' ] ],

            // All
            [ 'email', 'email', 'on' => [ 'sign-up', 'insert', 'update' ] ],
            [ [ 'name', 'email' ], 'unique', 'on' => [ 'sign-up', 'insert', 'update' ] ],
        ];
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public function getId() {
        return $this->id;
    }

    /** @param $identity Users */
    public function login($identity = null) {
        if(!$identity)
            $identity = $this::findOne([
                'name' => $this->name
            ]);
        Yii::$app->user->login($identity);
        $this->assignRole($identity);
    }

    /**
     * @param $identity Users
     * @param null $role
     * @param bool $force
     */
    public function assignRole($identity = null, $role = null, $force = false) {
        $am = Yii::$app->authManager;
        $identity = $identity == null ? $this : $identity;
        if($force)
            $am->revokeAll($identity->getId());
        $roles = $am->getRolesByUser($identity->id);
        if(count($roles) == 0) {
            $role = $am->getRole($role == null ? ($identity->role ?? 'user') : $role);
            $am->assign($role, $identity->getId());
        }
    }

    public function sendValidationEmail() {
        if($this->validation !== null)
            Yii::$app->mailer
                ->compose('validation', [
                    'user' => $this
                ])
                ->setFrom('no-reply@' . Yii::$app->params['host'])
                ->setTo($this->email)
                ->setSubject('Validation')
                ->send();
    }

    public static function getListOfRoles() {
        $roles = (new yii\db\Query)
            ->select('*')
            ->from('auth_item')->all();
        $return = [];
        foreach($roles as $role)
            $return[$role['name']] = $role['description'];
        return $return;
    }

    public function setScenario($scenario) {
        if($scenario == 'update')
            $this->old['password'] = $this->password;
        parent::setScenario($scenario);
    }

    public function beforeSave($insert) {
        if(!$insert) {
            if (mb_strlen($this->password) == 0)
                $this->password = $this->old['password'];
            else
                $this->password = Yii::$app->getSecurity()->generatePasswordHash($this->password);
        }
        return parent::beforeSave($insert);
    }

    public static function findIdentityByAccessToken($token, $type = null) {return null;}
    public function getAuthKey() {return null;}
    public function validateAuthKey($authKey) {return null;}
}